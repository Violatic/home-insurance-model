# How to Read:

The code for the initial investigation is contained within Home Insurance 
Model.ipynb. 

Graphs are added seperately from the notebook for ease of access, and also 
to not lag the notebook by having to load all of the PNG's in your browser.